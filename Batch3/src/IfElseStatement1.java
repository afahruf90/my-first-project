import java.util.Scanner;

public class IfElseStatement1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Apa yang kamu pilih? Batu/Gunting/Kertas?");
        String pilihanSatu = input.nextLine();
        System.out.println("Apa yang kamu pilih? Batu/Gunting/Kertas?");
        String pilihanDua = input.nextLine();

        if (pilihanSatu.equals("Batu") && pilihanDua.equals("Batu")){
            System.out.println("Serie. Batu hanya bisa dikalahkan Kertas");
        }
        else if (pilihanSatu.equals("Gunting") && pilihanDua.equals("Gunting")){
            System.out.println("Serie. Gunting hanya bisa dikalahkan Batu");
        }
        else if (pilihanSatu.equals("Kertas") && pilihanDua.equals("Kertas")){
            System.out.println("Serie. Kertas hanya bisa dikalahkan Gunting");
        }
        else if (pilihanSatu.equals("Batu") && pilihanDua.equals("Gunting")){
            System.out.println("Gunting KALAH");
        }
        else if (pilihanSatu.equals("Batu") && pilihanDua.equals("Kertas")){
            System.out.println("Batu MENANG");
        }
        else if (pilihanSatu.equals("Gunting") && pilihanDua.equals("Batu")){
            System.out.println("Gunting KALAH");
        }
        else if (pilihanSatu.equals("Gunting") && pilihanDua.equals("Kertas")){
            System.out.println("Gunting MENANG");
        }
        else if (pilihanSatu.equals("Kertas") && pilihanDua.equals("Gunting")){
            System.out.println("Kertas KALAH");
        }
        else if (pilihanSatu.equals("Kertas") && pilihanDua.equals("Batu")){
            System.out.println("Kertas MENANG");
        }
        else {
            System.out.println();
        }


    }

}

