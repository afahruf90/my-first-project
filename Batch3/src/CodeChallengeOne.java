import java.util.Scanner;

public class CodeChallengeOne {
    public static void main(String []args) {
        Scanner input = new Scanner(System.in);
        System.out.println("nama");
        String name = input.nextLine();
        System.out.println("nama saya " + name);
        System.out.println("alamat");
        String address = input.nextLine();
        System.out.println("alamat saya " + address);
        System.out.println("tinggi badan");
        String height = input.nextLine();
        System.out.println("tinggi badan saya " + height );
        System.out.println("umur");
        String age = input.nextLine();
        System.out.println("umur saya " + age);
        System.out.println("nomor sepatu");
        String nosepatu = input.nextLine();
        System.out.println("nomor sepatu " + nosepatu);
    }
}
